<?php

class tx_forcerealurls {

	/**
	 * @param $params
	 * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
	 */
	function check($params, $pObj) {
		if ($pObj->siteScript && $pObj->config['config']['tx_realurl_enable'] && (
				substr($pObj->siteScript, 0, 9) == 'index.php' ||
				substr($pObj->siteScript, 0, 1) == '?'
				)
				&& strpos($pObj->siteScript, '&noforce=1') === false // A way to skip redirect. Thanks goes to Reindl Bernd.
				&& strpos($pObj->siteScript, '&jumpurl=') == false // Skip redirect if URL is for jumpurl. Thanks goes to Reindl Bernd.
				&& !$pObj->isBackendUserLoggedIn() // Skip redirect if we are in BE Logged in
			) {
			$baseURL = $pObj->config['config']['baseURL'];
			$LD = $pObj->tmpl->linkData($pObj->page, '', $pObj->no_cache, '', '', \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('QUERY_STRING'));

			if (strtolower($LD['totalURL']) != strtolower($pObj->siteScript)) {
				$url = $baseURL . '/' . ltrim($LD['totalURL'], '/');
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $url);
				exit;
			}
		}
	}

}

?>